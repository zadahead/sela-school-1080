import { useState } from "react";

export const useColorSwitch = () => {
    const [isRed, setIsRed] = useState(true);

    const handleSwitch = () => {
        setIsRed(!isRed);
    }

    const color = isRed ? 'red' : 'blue';

    return {
        color,
        handleSwitch
    }
}