import axios from "axios";
import { useEffect, useState } from "react";

export const useFetch = (url) => {
    const [isLoading, setIsLoading] = useState(true);
    const [list, setList] = useState(null);

    useEffect(() => {
        console.log('mounted');

        setTimeout(() => {
            axios.get(url)
                .then(resp => {
                    setList(resp.data);
                    setIsLoading(false);
                })

        }, 1000);
    }, [])

    return {
        isLoading,
        list,
        setList
    }
}