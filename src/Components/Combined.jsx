import { useState } from "react"
import { Btn, Line, Rows } from "UIKit"

export const Combined = () => {

    const [count, setCount] = useState(0);

    const [isRed, setIsRed] = useState(true);


    const handleAdd = () => {
        setCount(count + 1);
    }

    const handleSwitch = () => {
        setIsRed(!isRed);
    }
    const styleCss = {
        color: isRed ? 'red' : 'blue'
    }

    return (
        <Rows>
            <h2 style={styleCss}>Combined, {count}</h2>
            <Line>
                <Btn onClick={handleAdd}>Add</Btn>
                <Btn onClick={handleSwitch}>Switch</Btn>
            </Line>
        </Rows>
    )
}