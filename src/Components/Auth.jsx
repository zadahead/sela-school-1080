import { Btn, Line } from "UIKit";
import { useState } from "react"

export const Auth = ({ name }) => {
    const [isLogin, setIsLogin] = useState(true);

    const handleLogin = () => {
        setIsLogin(true);
    }

    const handleLogout = () => {
        setIsLogin(false);
    }

    const renderForm = () => {
        if (isLogin) {
            return (
                <Line>
                    <h3>Welcome {name}</h3>
                    <Btn onClick={handleLogout}>Logout</Btn>
                </Line>
            )
        }

        return null;
    }

    return (
        <div>
            <h2>asdasd</h2>
            {renderForm()}
        </div>
    )
}