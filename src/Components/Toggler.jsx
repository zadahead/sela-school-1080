import { Rows, Btn } from "UIKit"
import Welcome from "./Welcome"
import { useState } from "react"

export const Toggler = ({ element }) => {
    const [isDisplay, setIsDisplay] = useState(true);

    const handleToggle = () => {
        setIsDisplay(!isDisplay);
    }

    const renderComponent = () => {
        if (isDisplay) {
            return element;
        }
        return null;
    }

    return (
        <Rows>
            <h3>Toggler</h3>
            <div>
                <Btn onClick={handleToggle}>Toggle</Btn>
            </div>

            {renderComponent()}
        </Rows>
    )
}