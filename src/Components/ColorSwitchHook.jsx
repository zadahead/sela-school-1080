import { useColorSwitch } from "Hooks/useColorSwitch";
import { Btn, Rows } from "UIKit"



export const ColorSwitchHook = () => {
    const { color, handleSwitch } = useColorSwitch();

    const styleCss = {
        backgroundColor: color
    }


    return(
        <Rows>
            <h2 style={styleCss}>Color Switch</h2>
            <Btn onClick={handleSwitch}>Switch</Btn>
        </Rows>
    )
}