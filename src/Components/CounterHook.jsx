import { useColorSwitch } from "Hooks/useColorSwitch";
import { useCounter } from "Hooks/useCounter";
import { Btn, Rows } from "UIKit"





export const CounterHook = () => {
    //logic
    const { count, handleAdd } = useCounter(10);
    const { color, handleSwitch } = useColorSwitch();
    //

    const handleChange = () => {
        handleAdd();
        handleSwitch();
    }
    const styleCss = {
        color
    }


    //render
    return (
        <Rows>
            <h2 style={styleCss}>Count, {count}</h2>
            <div>
                <Btn onClick={handleChange}>Add</Btn>
            </div>
        </Rows>
    )
    //
}