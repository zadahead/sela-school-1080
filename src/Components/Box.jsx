import './Box.css';

const Box = (props) => {
    const title = props.title;
    const info = props.info;
    const children = props.children;

    return (
        <div className='Box'>
            <h1>{title}</h1>
            <h3>{info}</h3>
            <div className='content'>
                {children}
            </div>
        </div>
    )
}

export default Box;