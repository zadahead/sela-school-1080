import axios from "axios";
import { useFetch } from "Hooks/useFetch";
import { useState } from "react"
import { useEffect } from "react"
import { Rows } from "UIKit"



export const Todos = () => {

    const { isLoading, list, setList } = useFetch('https://jsonplaceholder.typicode.com/todos')


    const handleToggleCompleted = (item) => {
        item.completed = !item.completed;
        setList([...list]);
    }

    return (
        <Rows>
            <h2>Todos list</h2>
            {isLoading && <h3>loading...</h3>}
            {list && (
                list.map(i => {
                    const styleCss = {
                        color: i.completed ? '#e1e1e1' : ''
                    }
                    return (
                        <h3
                            key={i.id}
                            style={styleCss}
                            onClick={() => handleToggleCompleted(i)}
                        >
                            {i.title}
                        </h3>
                    )
                })
            )}
        </Rows>
    )
}