import axios from "axios";
import { useState } from "react";
import { useEffect } from "react";
import { Btn, Line } from "UIKit";

export const LifeCycleComp = () => {
    const [count, setCount] = useState(0);
    const [title, setTitle] = useState('Hello');

    console.log('Render');

    useEffect(() => {
        console.log('Mounted');

        setTimeout(() => {
            setTitle('Goodbye')

            axios.get('https://jsonplaceholder.typicode.com/todos')
                .then(resp => {
                    console.log(resp.data);
                })
        }, 2000)

        return () => {
            console.log('UnMounted');
        }
    }, [])

    useEffect(() => {
        console.log('Updated', count);

        return () => {
            console.log('Before Changed', count)
        }
    })

    return (
        <Line>
            <h2>{title}, {count}</h2>
            <Btn onClick={() => setCount(count + 1)}>Add</Btn>
        </Line>
    )
}