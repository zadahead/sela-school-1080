import './Welcome.css';

const Welcome = (props) => {
    console.log(props);

    const name = props.name;
    const age = props.age;

    

    return (
        <>
        <h1 className='Welcome'>Welcome {name}, Age: {age}</h1>
        {props.children}
        </>
    )
}

export default Welcome;