import { welcomeContext } from "Context/welcomeContext";
import { useContext } from "react";
import { useState } from "react";
import { Btn, Line } from "UIKit"

export const Counter = () => {
    const { count, setCount} = useContext(welcomeContext);


    const handleAdd = () => {
        setCount(count + 1);
        console.log('setState')
    }




    return (
        <>
            <Line>
                <h2>Count, {count}</h2>
                <Btn onClick={handleAdd}>Add</Btn>
            </Line>
        </>
    )

}