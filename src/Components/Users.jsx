import { useFetch } from "Hooks/useFetch"
import { Rows } from "UIKit"

export const Users = () => {
    const { isLoading, list } = useFetch('https://jsonplaceholder.typicode.com/users');
    return (
        <Rows>
            <h2>Users List</h2>
            { isLoading && <h3>loading...</h3>}
            { list && list.map(i => {
                return <h3 key={i.id}>{i.name}</h3>
            })}
        </Rows>    
    )
}