import { LifeCycleComp } from "Components/LifeCycleComp"
import { Todos } from "Components/Todos"
import { Toggler } from "Components/Toggler"
import { Users } from "Components/Users"
import { welcomeContext } from "Context/welcomeContext"
import { useContext } from "react"
import { Rows } from "UIKit"

export const LifeCycle = () => {
    const { greeting, calc } = useContext(welcomeContext);

    return (
        <Rows>
            <h4>
                {greeting}
                {calc(10, 15)}
            </h4>
            <h1>Life Cycle</h1>
            <Users />
            <Todos />
        </Rows>
    )
}