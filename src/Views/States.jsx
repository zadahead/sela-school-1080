import { Auth } from "Components/Auth";
import { ColorSwitch } from "Components/ColorSwitch";
import { Combined } from "Components/Combined";
import { Counter } from "Components/Counter";
import { Toggler } from "Components/Toggler";
import Welcome from "Components/Welcome";
import { Btn, Input, Line, Rows } from "UIKit"
import { useState } from "react";

//state

export const States = () => {

    return (
        <Rows>
            <h1>States</h1>
            <Rows>
                <Counter />
                <Toggler element={<Counter />} />
            </Rows>
        </Rows>
    )
}