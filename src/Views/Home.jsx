import { Line, LinkBtn, Icon, Btn } from 'UIKit';

export const Home = () => {
    const handleClick = () => {
        console.log('clicked!!');
    }

    return (
        <div>
            <Line>
                <LinkBtn />
                <Icon i="home" />
                <Icon i="favorite" />
                <Btn i="home" onClick={handleClick}>CLICKKK</Btn>
                <Btn onClick={handleClick}>CLICKKK</Btn>
            </Line>
        </div>
    )
}