import { useState } from "react"
import { Dropdown, Rows } from "UIKit"

const list = [
    { id: 1, value: 'Mosh'},
    { id: 2, value: 'David'},
    { id: 3, value: 'Ruth'},
    { id: 4, value: 'Bracha'},
]
export const DropdownView = () => {
    const [selected, setSelected] = useState();

    return (
        <Rows>
            <h1>Drop down view</h1>
            <Dropdown list={list} selected={selected} onChange={setSelected} />
        </Rows>
    )
}