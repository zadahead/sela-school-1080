import { ColorSwitch } from "Components/ColorSwitch"
import { ColorSwitchHook } from "Components/ColorSwitchHook"
import { CounterHook } from "Components/CounterHook"
import { Rows } from "UIKit"

export const Hooks = () => {
    return (
        <Rows>
            <h1>Custom Hooks</h1>
            <CounterHook />
            <ColorSwitchHook />
        </Rows>
    )
}