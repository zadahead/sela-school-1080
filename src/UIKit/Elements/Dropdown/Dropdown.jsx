import { useEffect } from "react";
import { useState } from "react";
import { Between, Icon, Line } from "UIKit"

import './Dropdown.css';

export const Dropdown = ({ list, selected, onChange }) => {
    const [isOpen, setIsOpen] = useState(false);

    useEffect(() => {
        document.body.addEventListener('click', handleClose);

        return () => {
            document.body.removeEventListener('click', handleClose);
        }
    }, [])

    const handleClose = () => {
        console.log('handleClose');
        setIsOpen(false);
    }

    const handleToggle = (e) => {
        e.stopPropagation();
        setIsOpen(!isOpen);
    }

    const renderHeader = () => {
        if(selected) {
            const item = list.find(i => i.id === selected);

            if(item) {
                return item.value;
            }
        }
        return 'Please Select';
    }

    const handleSelect = (id) => {
        onChange(id);
        setIsOpen(false);
    }

    return (
        <div className="Dropdown">
            <div className="header" onClick={handleToggle}>
                <Between>
                    <h2>{renderHeader()}</h2>
                    <Icon i="expand_more" />
                </Between>
            </div>
            {isOpen && (
                <div className="list">
                    {list.map(i => {
                        return <h3 onClick={() => handleSelect(i.id)} key={i.id}>{i.value}</h3>
                    })}
                </div>
            )}
        </div>
    )
}