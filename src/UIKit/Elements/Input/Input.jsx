import './Input.css';

export const Input = ({ value, onChange, isNumber, isUpper }) => {

    const handleInputChange = (e) => {
        let val = e.target.value;
        //val='0f'
        if (isNumber && isNaN(val)) {
            return;
        }
        if (isUpper) {
            val = val.toUpperCase();
        }
        onChange(val);
    }


    let val = value;

    if (isNumber && (!val || isNaN(val))) {
        val = 0;
    }

    if (isUpper) {
        val = val.toUpperCase();
    }

    return (
        <div className='Input'>
            <input value={val} onChange={handleInputChange} />
        </div>
    )
}