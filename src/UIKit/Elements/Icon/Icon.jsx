import './Icon.css';

export const Icon = ({ i }) => {

    return (
        <div className="Icon">
            <span className="material-symbols-outlined">{i}</span>
        </div>
    )
}

export default Icon;