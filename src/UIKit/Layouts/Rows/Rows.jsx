import './Rows.css';

export const Rows = (props) => {
    return (
        <div className='Rows'>
            {props.children}
        </div>
    )
}

export default Rows;