import Box from "./Components/Box";
import Welcome from "./Components/Welcome";
import Line from "./UIKit/Layouts/Line/Line";
import Rows from "./UIKit/Layouts/Rows/Rows";

import './App.css';

import { Btn, LinkBtn, Between, Grid, Icon } from 'UIKit';
import { Link, NavLink, Route, Routes } from "react-router-dom";
import { Home } from "Views/Home";
import { About } from "Views/About";
import { States } from "Views/States";
import { LifeCycle } from "Views/LifeCycle";
import { Hooks } from "Views/Hooks";
import { useContext } from "react";
import { welcomeContext } from "Context/welcomeContext";
import { DropdownView } from "Views/DropdownView";


//https://gitlab.com/zadahead/sela-school-1080
//http://zadahead.com/React/61dc6604dcbade1da8590b3d
//http://zadahead.com/React/61dc6c34dcbade1da8590b45

const App = (props) => {
    const { count } = useContext(welcomeContext);

    return (
        <>
            <Grid>
                <div>
                    <Between>
                        <Line>
                            <Icon i="menu" />
                            <h3>Sels 1080</h3>
                            <h4>{count}</h4>
                        </Line>
                        <Line>
                            <NavLink to='/dropdown'>Dropdown</NavLink>
                            <NavLink to='/hooks'>Hooks</NavLink>
                            <NavLink to='/cycle'>Cycle</NavLink>
                            <NavLink to='/states'>States</NavLink>
                            <NavLink to='/about'>About</NavLink>
                            <NavLink to='/home'>Home</NavLink>
                        </Line>
                    </Between>
                </div>
                <div>
                    <div>
                        <Rows>
                            <Routes>
                                <Route path="/home" element={<Home />} />
                                <Route path="/about" element={<About />} />
                                <Route path="/states" element={<States />} />
                                <Route path="/cycle" element={<LifeCycle />} />
                                <Route path="/hooks" element={<Hooks />} />
                                <Route path="/dropdown" element={<DropdownView />} />
                            </Routes>
                        </Rows>
                    </div>
                </div>
                <div>
                    <Line>
                        <h3>Sels Group</h3>
                    </Line>
                </div>
            </Grid>

        </>
    )
}

export default App;

// <Between>
//                     <Line>
//                         <h4>pic</h4>
//                         <Rows>
//                             <h4>auto complete</h4>
//                             <Line>
//                                 <h4>ZadaheaD</h4>
//                                 <h4>auth 1 week ago</h4>
//                             </Line>
//                         </Rows>
//                     </Line>
//                     <Line>
//                         <h4>icon1</h4>
//                         <h4>icon2</h4>
//                     </Line>
//                 </Between>